<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '../../../../vendor/autoload.php';

use Centersis\Financeiro\Testes\GerarBoleto;
use Centersis\Financeiro\Retorno\LerRetorno;

try {
    $gerarBoleto = new GerarBoleto();

    echo $gerarBoleto->gerar(1);
    
} catch (\Exception $e) {
    echo $e->getMessage();
    echo '<br>';
    echo $e->getTraceAsString();
}

$caminhoArquivo = 'retorno_bradesco_400.ret';

try {
    $dadosLeitura = (new LerRetorno())->lerArquivo($caminhoArquivo);

    echo '<pre>';
    print_r($dadosLeitura);
} catch (\Exception $e) {
    echo $e->getMessage();
    echo '<br>';
    echo $e->getTraceAsString();
}