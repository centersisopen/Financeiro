<?php

namespace Centersis\Financeiro\Boleto\Bancos;

class Sicredi extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigoBancoComDv = '748-X';
        $nummoeda = '9';

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);

        $posto = $this->formataNumero($dadosConvenio["fnc_convenio_posto"], 2, 0);

        $conta = $this->formataNumero($dadosConvenio["fnc_convenio_conta"], 5, 0);

        //fillers - zeros Obs: filler1 contera 1 quando houver valor expresso no campo valor
        $filler1 = 1;
        $filler2 = 0;

        $tipoCobranca = 1;

        $tipoCarteira = $dadosConvenio["fnc_convenio_carteira"];

        $nnumComDv = $dadosLancamento["fnc_parcela_nosso_numero"];

        $campoLivre = "$tipoCobranca$tipoCarteira$nnumComDv$agencia$posto$conta$filler1$filler2";
        $campoLivreDv = $campoLivre . $this->digitoVerificadorCampoLivre($campoLivre);

        $dv = $this->digitoVerificadorBarra("$codigobanco$nummoeda$fatorVencimento$valor$campoLivreDv");

        $linha = "$codigobanco$nummoeda$dv$fatorVencimento$valor$campoLivreDv";

        $nossonumero = substr($nnumComDv, 0, 2) . '/' . substr($nnumComDv, 2, 6) . '-' . substr($nnumComDv, 8, 1);
        $agenciaCodigo = $agencia . "." . $posto . "." . $conta;

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agenciaCodigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigoBancoComDv;
        $dadosboleto["campo_livre"] = $campoLivreDv;

        return $dadosboleto;
    }

    public function digitoVerificadorNossoNumero($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);

        $digito = 11 - $resto2;
        if ($digito > 9) {
            return 0;
        } else {
            return $digito;
        }
    }

    protected function digitoVerificadorCampoLivre($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);

        if ($resto2 <= 1) {
            return 0;
        } else {
            return (11 - $resto2);
        }
    }

    protected function digitoVerificadorBarra($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);
        $digito = 11 - $resto2;

        if ($digito <= 1 || $digito >= 10) {
            return 1;
        } else {
            return $digito;
        }
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        for ($i = strlen($num); $i > 0; $i--) {
            $numeros[$i] = substr($num, $i - 1, 1);
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    protected function modulo11($num, $base = 9, $r = 0) {
        $soma = 0;
        $fator = 2;

        for ($i = strlen($num); $i > 0; $i--) {

            $numeros[$i] = substr($num, $i - 1, 1);
            $parcial[$i] = $numeros[$i] * $fator;
            $soma += $parcial[$i];

            if ($fator == $base) {
                $fator = 1;
            }

            $fator++;
        }

        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;
            return $digito;
        } elseif ($r == 1) {
            $rDiv = (int) ($soma / 11);
            $digito = ($soma - ($rDiv * 11));
            return $digito;
        }
    }

    protected function montaLinhaDigitavel($codigo) {
        $p1 = substr($codigo, 0, 4);
        $p2 = substr($codigo, 19, 5);
        $p3 = $this->modulo10("$p1$p2");
        $p4 = "$p1$p2$p3";
        $p5 = substr($p4, 0, 5);
        $p6 = substr($p4, 5);
        $campo1 = "$p5.$p6";

        $p1 = substr($codigo, 24, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo2 = "$p4.$p5";

        $p1 = substr($codigo, 34, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo3 = "$p4.$p5";

        $campo4 = substr($codigo, 4, 1);

        $p1 = substr($codigo, 5, 4);
        $p2 = substr($codigo, 9, 10);
        $campo5 = "$p1$p2";

        return "$campo1 $campo2 $campo3 $campo4 $campo5";
    }

    public function nossoNumero($numero, $dadosConvenio) {
        $nn = date('y') . $dadosConvenio['fnc_convenio_byte_idt'] . $this->formataNumero($numero, 5, 0);

        $agrupamento = $dadosConvenio['fnc_convenio_agencia'] . $dadosConvenio['fnc_convenio_posto'] . $dadosConvenio['fnc_convenio_conta'] . $nn;

        return $nn . $this->digitoVerificadorNossoNumero($agrupamento);
    }

    public function logo() {
        return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAyADIAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQECAQEBAQEBAgICAgICAgICAgICAgICAgICAgICAgICAgICAgL/2wBDAQEBAQEBAQICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgL/wAARCAAoAIcDAREAAhEBAxEB/8QAHQABAAIDAAMBAAAAAAAAAAAAAAgJBQYHAQMECv/EAC8QAAAGAgEDAwMDBAMAAAAAAAIDBAUGBwEIAAkREhMUIRUWFyJWkhgjUdMkMqP/xAAXAQEBAQEAAAAAAAAAAAAAAAAAAgED/8QAKxEBAAIBAgQDCAMAAAAAAAAAAAERAiFBEjFRYSIy8ANxkaGxweHxQoHR/9oADAMBAAIRAxEAPwD9/HAcCOb1tbR7FsKl1bVSwxRdZleIbYdIs3NDq4J4rXTrIjYi0yqZPqUoSFrIWOhXsUgFigo9QaMIiiRkYMNLziiylR1ZrHVX1G+rBrre0mmNlZjFeU9t1qoXM5fJ3BugdZT6t3aFTCNQpjErAhSo2eVIFWSPRS+ZY3HuM0QsFZBz/lPu9fVc+WE1Abm2A7b5NullYwOPzNkrrUNo2Ov+dL3leS+RV8mEr+16nrJmSh/45jlIE6J3cvUWGBCQnSlGC8gn+QKvWuya8NrEWB9bZKzoH1oPyobnIn1k4xAEUYHxHko4k4ofyAwsYRFmAz8gMAIOfnGeWxmOA4DgOA4DgOA4DgOA4DgOBRQ2xx86cGxW5d8bPQt1vWgN07Ab5pOdzIs0OD7KqAhbBG8RSEUVfdSNQVChFXkUb8KANMyjgVacgS5Yqlje3ZNy6c564zPSd1+aq2fJbNN2dK+oHqdtdQQEV0Vk+ae7G63XDOIbMYg45ldGTwlJOddrCZVJywkh99lJyTG5yMQnHDAWtKdc9k6rsHNeLrp8v2XWMw1LpRkSSatu9e2tqJHarbE2y2qsNRYhk3TrIa80frTrKX+CakhjydIgJhN609E1uS9MIX9glO4KnLBoskF+qw5GcxpHSFyNG7D0HeDccRQ83aZxHI/75rSO8Zbnr7PWkR1WBnWjicpUpi214SEnCAT7xpVLUghfATxfPLjLGUcnR5FO21hXAZk7c9yaQmEAWfb8YQBXuCdCYMRZa5yPUGEpEhQxBEEsStST62QjwRgzIB+NWMF+TzEbqxNMjgE6jJkidiGNuXLSI26teXFUQapTEqFkZcl2S/IBJucjGDAC/H+4IPfHMHVOaHAcBwHAcBwHAcBwObXJNXitaitKxo9HQS99gNdzSas8TMXGtYZO5RaOKXxHH/qRBCoZGVgyMJ8HBTKMl5MwPBJnbxzkzUCMepGzU/3F1aoraGKQ2t49FL6qyL2SijT1MX50Xx1PJmoKxdHXhegafbHHITcmpFAwYCDJhQ/0h/64mJyyhsxES6LrVD6zhSSYMFZVNW9YMzY8d8YqPAPx46qHxWpkrxlhClQN6Qk4DiqVmryEZGS8HngGYPJg/AqoZKHk31oPvzdqzJxYty07shUkLjNbMdX6bTGTOGY7Q9osaE9znU5nMEjxitvfXN5JVoD0IZI1nHM6YsJiDBYFWR5momeqrqFgEai03JmLG9PaCBMjDHYc8xZvaIkpeFGQhcF7aoQlpyFiRGQmTpy0IgYAAI++BFhCEAQfNpU0zTYQrVWzuoJWu6LJdbfRd7WGutiBbo0CzyabN0Sq1dWrHEnGrbTUUx7yUwdxhImpQSlcVDeS3rmc/wB6Q4ELfc+pyupm1MqOp6y1bdtWYzrHJJu+y3dix4VHq6lr9dF9T5vTUuW3AtS97EZ4HbskekTY9/Y6VSlRPiEpKpKy5EF+ilOAHCjaiOW/vbc5XeyYEQthC9bBbc1NHXiWpqw1ii1Yq7JVoZZI3eWOc5lsZVTJDEoq4Oqw0xEAptLwc6nEnJjVBypsTANIAmXDN2KuUoiXXsJtY0I4dEIpqna+ssytS56PqeB2hPdmK8txkc0FlzxKgmYUEFgcneVY1COMBenP11SYlGjG3mHlrcmJisjyZn1KoiEnLftodKbCw6Ey2pF7TqUuZ1CSx9x5htu9Qlkq+wjkIjopDHCEyJQXhy+tKjG9pRCRu2XAbov8RtoSCgHqN3/MpYSI3JIJP/W3ZkwVqldRazyNJVNHNEktgVKo5U/s0PTTGSKJjYz2vSJE2V699ZmBMvexYEh9kZ64RKTlARruyaRXaNq7ytt6pONRKtGSiHCzth66q9HKobuew7eN0whTgzPswu9A2igan6e2rWiLMDgd66owSlIpWt6tKAJpYRcy/r1ttQm9F7ejr3u7K9PFFPXYxtjTTy+3YPsAVczy6Qiafa0naobOozliSPGXVoWtTm7J06c1wT4Tugky8SQYsJB+W77srRpNG2jONhmnaGePUlflUPgd8KNX9dMt7o7xg5yWQnLfDpZYrmcwKSC3U9ZJlyonucD0wBZBkgBgIzRD2NSdFruMZxjGM575xjGM5/zn/PKY88BwNBtSQRuK1tOpDMCVSmKtUVe1MhTIUQHJYpaPYDLXJ0yAz9Jwxl5yHBY84ALOews+PfiRR3pfemieuHTdHKIladv3DpRpc3zmKr5pLW+HImxixFV4ZS4RGRMzIrQ/XHJMe7J2ttRiSmFHjNQt6VIYqzk07ljONb6LyieJHC9Nt+oFrVoXsB1DZMpf4TbVqqIlaFLaySVoQPFWada2OktY6lqqF2BDwHIBnS99TvP3BJD8HqMI30rDSQmTJkSj1Zyyyxxv5dGxjE5U6PPXA/p67iSvYmZsb046S7cWdEobunMxNCJobdedrTmptYKR29ZHZjU+7a2KXNbgzRibmgAmSNLoQjdvVCkMUANrl9/W3rdkRcd1zL1dbjW7HZpj2ml8niUFIIRs9nt0faHUQl6lDgJiFyQFLUonRQiUnJCPURJi/fHGCTeHuSzBjviQrfqSlepjTcSglaMD5qdLoZCkKdH96WFqtZTlbj+ccaNe+SR9NS2cFCNzWKjjzzMgOJJyIzPbBQf0Y51lH6XM4d223PRe8Elu3XHYKsZNAnKyaTg1rxZ5UXlST+piqt1tY1nSLnquofWs5S/SAJG1sPQgAtcXFQowvNEoUm+IPCtb/DImGFnere19e7AqNrdKpKTGLTs6DsEI2vq3YetxPFC7FHxRatXRKyUYK7kWHiLSdqw5OCApUn+oo1rQaQiXpzDEgFA812+ZExWrT7I1l35m9g613oxo9VoRbWvEptWRN8Qb9eZufTKs6xq+LgaR4MPbZkgfnB3QFnOgy1CkSJEUBUTglEI8sagWeL1DYnHu+mWDW6g7MtNnXMRIBa27yv0HiuzrrM29CghNM7mMIUTTSGwsIJWOLqSxR97ClQRNzCoNB9LcmuGu555mROynm8p7Tz9/+HOO+33emqNf9/Hmu7TqdhuHpg3pXb9cdqzSdIrQoG07JHK3iyLJW2gicJukaJiFjVmZJWIhpQkIgp0xaZOnCEJ6Mfgjj6weDu3HOgO4sPmWu9vUvIOnxWtkVJHbibpDEI5rZYsNozD7ZxyFsSyeDQyDSZCrLcCWdIpSK1zosVnKBOiossBKcokPN4cr2Lx7/FKKmab30Qz5a+3TYekrLGXfPuZYbrprtP4tZU7GERx2Gh5m07lTkSmTjOOyccpw3rVmfI7CcaU433QNrPsyeHa2saM6nbW0BG2Ss76tTX2b1fBZLKp3ESqlrOdROcSScTSYPMxdnixnyUvq9EaWSpeBmkFN7cmEI4kjIzAlkhANjjluZTjKz/lpOA4GKfGNmkzM5x6QtiJ6YnpCobXZpck5atA4oFZeSVKRWmOxkIyxhznAg5x2zjidRTj1BenyGRNmuNka6UrAp1EdX7ySX7ZWlDeU2V1GdpgsbN7KP5Ke03pIRySKqSyX2Mtj2H7eenVIlSPZycBSNUmjKP0vGevxcN66cVOsDpUbiWo7QGRIJa7E0O0wxgfGswUzj8daLqi5hqE9pZjVIMHiXqnU0zBQ1JeSshOLNGV4CxPtfIez88f39FpJNZUdfEgnDRLYHFbcgcyrmGt0txIGPMihDqvbAntq6OuWHAA2xyCckyiHlOHCjBWUwsn+ORFcusZTcw7EHXOjQtgWX8YRMTMAIAgaDW/1mwOCjsKSeyA0WSu4DcYNLz49wGhCYDsMIRY2oLllM0fVGfnMJas5z85zkS35/wDXioYfg+p/2S0/yWf7eKgPwfU/7Jaf5LP9vFQH4Pqf9ktP8ln+3ioGGkGtlBy5lc41MKjgkwjT2hVNrzG5ewI5THnZAtIEmVJHJjf8KEp4BgEIOQmlCx8/HbPHDA2urqjqykYg319TldQqrYM1CUjbIhAIy0RONoBLFQ1ysSRnZCiSAZMONMNH4gx5DGIWfkWebVDonAcBwHAcD//Z"';
    }

}
