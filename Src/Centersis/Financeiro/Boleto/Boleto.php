<?php

namespace Centersis\Financeiro\Boleto;

class Boleto {

    protected $bancos;

    public function __construct() {

        $this->bancos = [
            '001' => 'BancoBrasil',
            '033' => 'Santander',
            '041' => 'Banrisul',
            '085' => 'Ailos',
            '104' => 'Caixa',
            '136' => 'Unicred',
            '237' => 'Bradesco',
            '341' => 'Itau',
            '422' => 'Safra',
            '748' => 'Sicredi',
            '756' => 'Sicoob',
        ];
    }

    protected function getDadosBoleto($dadosLancamento, $dadosConvenio) {

        $classBanco = $this->instanciaDinamica($dadosConvenio);

        return $classBanco->dadosBoleto($dadosLancamento, $dadosConvenio);
    }

    protected function nossoNumero($numero, $dadosConvenio) {

        $classBanco = $this->instanciaDinamica($dadosConvenio);

        return $classBanco->nossoNumero($numero, $dadosConvenio);
    }
    
    private function instanciaDinamica($dadosConvenio) {
        $numeroBanco = $dadosConvenio['fnc_convenio_banco'];

        if (!key_exists($numeroBanco, $this->bancos)) {
            throw new \Exception('Número do banco não encontrado!');
        }

        $caminhoClass = 'Centersis\\Financeiro\\Boleto\\Bancos\\' . $this->bancos[$numeroBanco];
        return new $caminhoClass;
    }

}
